const mongoose = require("mongoose");

const TaskSchema = new mongoose.Schema({
  DeveloperName: {
    type: String,
    require: true,
  },
  DepartmentName: {
    type: String,
    require: true,
  },
  AssignedProjects: {
    type: Array,
    require: true,
  },
  creater: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
});

const Task = mongoose.model("AllTasks", TaskSchema);

module.exports = Task;
