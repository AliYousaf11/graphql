const express = require("express");
const app = express();
const DB = require("./config/DB");
require("dotenv").config();
const port = process.env.PORT || 4000;
const bodyParser = require("body-parser");
const graphqlHttp = require("express-graphql");

// resolver & schema
const schema = require("./graphQl/schema/index");
const rootValue = require("./graphQl/resolvers/index");

// middleware
// extract the body from the incomming request.
app.use(bodyParser.json());

app.use(
  "/graphql",
  graphqlHttp.graphqlHTTP({
    schema,
    rootValue,
    graphiql: true,
  })
);

// sevrer start at.
app.listen(port, () => {
  console.log(`server start at : http://localhost:${port}/graphql`);
});
