const mongoose = require("mongoose");
require("dotenv").config();
const colors = require("colors");

mongoose.connect(process.env.DB_URL);
const connection = mongoose.connection;

connection.on("connected", () => {
  console.log("mongoDb connected..".bgGreen);
});

connection.on("error", (error) => {
  console.log(`mongoDb error.. ${error}`.bgRed);
});

module.exports = mongoose;
