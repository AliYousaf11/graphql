const User = require("../../model/User");
const TaskModel = require("../../model/Task");

module.exports = {
  // Gel all user ...
  GetAllUsers: async () => {
    try {
      const users = await User.find();
      return users.map((user) => {
        return {
          ...user._doc,
        };
      });
    } catch (error) {
      console.error(error);
      throw error;
    }
  },

  // Register new User
  Register: (args) => {
    const { name, email, password, phoneNumber } = args.user;
    const newUser = new User({
      name,
      email,
      password,
      phoneNumber,
    });
    newUser
      .save()
      .then((res) => {
        console.log("new user saved");
      })
      .catch((error) => {
        console.log("error");
      });
    return args.user;
  },

  // assign new tasks
  Task: async (args) => {
    const { DeveloperName, DeparmentName, AssignedProjects, creater } =
      args.task;

    // Task Model
    const newTask = new TaskModel({
      DeveloperName,
      DeparmentName,
      AssignedProjects,
      creater,
    });

    // Creater Model / User Model
    const userModel = await User.findOne({ _id: creater });
    userModel.TasksAssignedTo.push([newTask._id]);

    // save the new Task
    userModel.save();
    newTask
      .save()
      .then((res) => {
        console.log("new Task saved");
      })
      .catch((error) => {
        console.log("error");
      });
    return args.task;
  },

  CheckTask: async (args) => {
    try {
      const userModel = await TaskModel.findOne({ _id: args.Dev.id });

      const { DeveloperName, DepartmentName, AssignedProjects, creater } =
        userModel;

      console.log({ DeveloperName, DepartmentName, AssignedProjects, creater });
      return {
        DeveloperName: userModel.DeveloperName,
        DepartmentName: userModel.DepartmentName,
        AssignedProjects: userModel.AssignedProjects,
        creater: userModel.creater,
      };
    } catch (error) {
      console.error(error);
      throw error;
    }
  },
};
