const { buildSchema } = require("graphql");

module.exports = buildSchema(`
  schema{
    query:RootQuery
    mutation:RootMutation
  }
  input task{
    DeveloperName : String!
    DeparmentName : String!
    AssignedProjects: [String!]!
    creater: String!
  }
  input DevInfo{
    id:ID!
  }
  input user{
    name : String!
    email: String!
    password:Int!
    confirmPassword:Int!
    phoneNumber:Int!
  }
  type userDetails {
    name:String!
    email:String!
    password:Int!
    phoneNumber:Int!
  }
  type usertask{
    DeveloperName : String!
    DeparmentName : String!
    AssignedProjects: [String!]!
    creater: String!
  }
  type RootQuery{
    GetAllUsers : [userDetails!]!
    CheckTask(Dev:DevInfo):usertask!
  }
  type RootMutation{
      Register(user:user):userDetails
      Task(task:task):usertask
  }
    
`);

